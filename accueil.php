<?php include "includes/bdd/db_connection.php"?>
<?php include "includes/bdd/show_veilles.php"?>
<?php include "includes/bdd/show_today_veille.php"?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Accueil</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />
    <link href="https://fonts.googleapis.com/css2?family=Pacifico&family=Parisienne&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Mr+Dafoe&family=Petit+Formal+Script&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/accueil.css">
</head>
<body>
    <!-- /headerwrap -->
    <header>
        <nav class="nav">
           <img src="/assets/img/leo.png" alt="">
          <ul class="nv">
              <li><a href="contact.php">Contact</a></li>
              <li><a href="rss.php">Flux RSS</a></li>
        </ul>
        <img src="/assets/img/leo1.png" class="img2" alt="">
    </nav>
</header>
<div class="container">
    <div class="text-center mt-5">
        <h3 class="text-center">Articles du jour</h3>
    </div>
    <div class="row">

        <?php 
            for ($i=0; $i < sizeof($veille); $i++) { 
            ?>
                <div class="col-12 col-lg-3">
                    <div class="cardBox mt-5 p-3" style="background: url(<?php echo $veille[$i]["image_url"] ?>) center / cover">
                        <div class="card-footer rounded mt-3">
                            <p class="rounded mt-5 p-2 text-white"><?php echo $veille[$i]["date"] ?></p>
                            <div>
                                <?php echo utf8_encode($veille[$i]["topic"]); ?>
                            </div>
                            <a class="btn btn-info mt-3" href="veille.php?id=<?php echo $veille[$i]["id"]?>">Consulter</a>
                        </div>
                    </div>
                </div>
            <?php
            }
            ?>
    </div>

    <div class="text-center mt-5">
        <h3>Articles</h3>
    </div>
    <div class="row">
            <?php 
            for ($i=0; $i < sizeof($veilleRss); $i++) { 
            ?>
                <div class="col-12 col-lg-3">
                    <div class="cardBox mt-5 p-3" style="background: url(<?php echo $veilleRss[$i]["image_url"] ?>) center / cover">
                        <div class="card-footer rounded mt-3">
                            <p class="rounded mt-5 p-2 text-white"><?php echo $veilleRss[$i]["date"] ?></p>
                            <div>
                                <?php echo utf8_encode($veilleRss[$i]["topic"]); ?>
                            </div>
                            <a class="btn btn-info mt-3" href="veille.php?id=<?php echo $veilleRss[$i]["id"]?>">Consulter</a>
                        </div>
                    </div>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
<script src="../assets/js/function.js"></script>
<script src="../assets/js/models/article.js"></script>

</body>
</html>