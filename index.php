<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Accueil</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />
    <link href="https://fonts.googleapis.com/css2?family=Pacifico&family=Parisienne&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Mr+Dafoe&family=Petit+Formal+Script&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
    <div class="container">
    <div class="menu" tabindex="1">
        <div class="list">
        <ul>
            <li><a href="#">Blog</a></li>
            <li><a href="#">À propos</a></li>
            <li><a href="#">Nous contacter</a></li>
            <button type="button" class="btn btn-success"><a href="login.php">Login</a></button>
        </ul>
        </div>
        <div class="intro">
        <div class="btne"></div>
        <img src="./assets/img/Feedly-logo.png" class="image"/>
        <h1 class="color">Goodbye information overload</h1>
        <p class="color">Keep up with the topics and trends you care about, without the overwhelm</p>
        <button type="button" class="btn btn-success">Get Start For Free</button>
        </div>
    </div>
    </div>  
<?php include "includes/temp/js.php"; ?>
<?php include "includes/temp/footer.php"; ?>
