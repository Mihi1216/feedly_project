function getAll(){
    var controller = "article";
    var method = "getAll";
    console.log("article getAll");
    ajax(controller, method, {}, "POST").done(function(resp){
        var data = JSON.parse(resp);
        console.log("data", data);
        data.forEach(element => {
            $('#mesArticles').append('<div class="col" ontouchstart="this.classList.toggle("hover"); "><div class="container"><div class="front" style="background-image: url('
            + element.image_url +')"><div class="inner"><p>'+ element.date +'</p><span>'
            + element.synthesis +'</span></div></div><div class="back"><div class="inner2"><br><a href="'+ element.link +'">Sources</a></div></div></div></div>');
        });
    });
}
function getById(id){
    var controller = "article";
    var method = "getById";
    console.log("article getById");
    
    ajax(controller, method, { id: id }, "GET").done(function(resp){
        var data = JSON.parse(resp);
        console.log("data", data);
        
        $("#selectedflux").html(data.name);
            
    });
}