function getAll(){
    var controller = "user";
    var method = "getAll";
    console.log("user getAll");
    ajax(controller, method, {}, "POST").done(function(resp){
        var data = JSON.parse(resp);
        console.log("data", data);
        data.forEach(element => {
            $("#flux").append("<li onclick='getById(" + element.id + ")'>" + element.name + "</li>");
        });
    });
}