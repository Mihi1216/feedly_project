function getAll(){
    var controller = "flux";
    var method = "getAll";
    console.log("flux getAll");
    ajax(controller, method, {}, "POST").done(function(resp){
        var data = JSON.parse(resp);
        console.log("data", data);
        data.forEach(element => {
            $("#flux").append("<li onclick='getById(" + element.id + ")'>" + element.name + "</li>");
        });
    });
}

function getById(id){
    var controller = "flux";
    var method = "getById";
    console.log("flux getById");
    
    ajax(controller, method, { id: id }, "GET").done(function(resp){
        var data = JSON.parse(resp);
        console.log("data", data);
        
        $("#selectedflux").html(data.name);
            
    });
}