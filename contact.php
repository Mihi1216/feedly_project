<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>contact</title>
    <hearder>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css">
        <link rel="stylesheet" type="text/css" href="assets/css/accueil.css">
    </hearder>
</head>
<body>
<?php include "includes/temp/header.php"; ?>
<div class="container">
    <div class="row">
        <div class="col-12 col-lg-6">
            <div class="card mb-3 text-center mt-5" style="max-width: 290px;">
                <div class="card" style="width: 30rem;">
                    <img src="assets/img/profil.jpg" id="top" alt="brenda">
                    <div class="card-body">
                        <h5 class="card-title">Kong Brenda</h5>
                        <p class="card-text">Profession: Blogeuse</p>
                        <p class="card-text">Tél: 87 24 55 88</p>
                        <p class="card-text">Adresse: Punaauia</p>
                        <p class="card-text">Email: brendakong26@gmail.com</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6">
            <div class="card mb-3 text-center mt-5" style="max-width: 290px;">
                <div class="card" style="width: 30rem;">
                    <img src="assets/img/profil.jpg" class="card-img-top" alt="mihiana">
                    <div class="card-body">
                        <h5 class="card-title">Maitui Mihiana</h5>
                        <p class="card-text">Profession: Blogeuse</p>
                        <p class="card-text">Tél: 87 23 24 25</p>
                        <p class="card-text">Adresse: Paea</p>
                        <p class="card-text">Email: mihiana@mail.pf</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="text-center mt-5" style="background-color: #fff; border-radius: 5%">
    <h6>Vous êtes passionné de jeux vidéo, de cinéma, de cuisine, de lecture… Pourquoi ne pas devenir blogueur ? Encore confidentiel, ce métier dédié à la production de contenus web semble avoir le vent en poupe. Mais derrière une apparente facilité d’accès se cache pourtant une activité professionnelle peu rémunératrice.</h6><br>
        <p>Outre la maîtrise de l'outil internet, de la technologie informatique et des réseaux sociaux, le blogueur doit avant tout posséder des qualités rédactionnelles, à la différence du youtubeur avec lequel il est souvent, à tort, associé et qui, lui, génère principalement des contenus vidéos. En effet, la production de contenus se rapproche beaucoup du métier de journaliste internet puisqu'il s'agit, pour le blogueur, d'écrire quasi quotidiennement des articles sur des sujets divers et variés dont le style sera adapté à la cible du blog.
        <br>D'autre part, il est impératif que vous ayez un esprit indépendant relativement prononcé. En effet, la plupart des blogueurs pratiquent leur activité en freelance et doivent, pour la majorité, exercer une autre profession afin de subvenir à leurs besoins. A ce titre, vous devez être suffisamment autonome et assidu pour travailler efficacement à domicile. Toujours en quête de popularité, le blogueur se doit également d'être en perpétuelle quête de nouveautés ou de thèmes "tendance". Il doit disposer d'un solide sens de l'analyse et de l'anticipation. En bref, le blogueur doit être capable de lancer des modes ou de susciter l'intérêt de sa cible sur un sujet donné.</p>
    </div>

<h1 class="text-center mt-6">Compétences</h1>
    <div class="row">
    <div class="col-4 col-lg-3 mt-5">
    <img class="w-100" style="border-radius: 10%" src="assets/img/cms.jpg" alt="profil">
    <h4 class="text-center mt-3">Gestion de sites web par CSM</h4>
    </div>
    <div class="col-12 col-lg-3 mt-5">
    <img class="w-100" style="border-radius: 10%" src="assets/img/analytics.jpg" alt="profil">
    <h4 class="text-center mt-3">Analyse des données</h4>
    </div>
    <div class="col-12 col-lg-3 mt-5">
    <img class="w-100" style="border-radius: 10%" src="assets/img/design.jpg" alt="profil">
    <h4 class="text-center mt-3">Création de visuels</h4>
    </div>
    <div class="col-12 col-lg-3 mt-5">
    <img class="w-100" style="border-radius: 10%" src="assets/img/socialmedia.jpg" alt="profil">
    <h4 class="text-center mt-3">Gestion des R-S</h4>
    </div>
    </div>

        <div class="row mt-5">
            <div class="col-12 mt-5">
                <form>
                    <fieldset>
                        <legend class="text-center">Contactez nous pour tout renseignements ou demande de devis</legend>

                        <div class="form-group">
                            <label for="nom">Nom</label>
                            <input type="text" class="form-control" id="nom" placeholder="">
                        </div>

                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="message">Message</label>
                            <textarea class="form-control" id="bio" rows="3"></textarea>
                        </div>
                    </fieldset>
                    <button class="btn btn-info mt-3 mb-5">Envoyer</button>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
