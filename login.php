<?php include "verification.php" ?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css">
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <title>Login</title>
</head>
<body>
<div class="background-login text-center">
    <div class="card-log">
        <h1 class="mb-3 mt-3">Connexion</h1>
            <form action="user.php" method="POST">
                <div class="mb-3">
                <input type="email" name="email" id="email" placeholder="Email" required="">
                </div>
                <div class="mb-3">
                    <input type="password" name="password" placeholder="Mot de passe" required="">    
                </div>
                <button id="btn-submit" type="submit" name="submit" id="submit" class="btn btn-outline-success mt-1">Valider</button>
                <?php
                if(isset($_GET['erreur'])){
                    $err = $_GET['erreur'];
                    if($err==1 || $err==2)
                        echo "<p style='color:red'>Utilisateur ou mot de passe incorrect</p>";
                }
                ?>
            </form>
    </div>
</div>
</body>
</html>